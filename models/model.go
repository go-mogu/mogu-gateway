package models

import (
	"github.com/go-mogu/mogu-gateway/pkg/util"
)

type Model struct {
	Id        uint64          `gorm:"primaryKey;autoIncrement;column:id;type:bigint unsigned;NOT NULL;" json:"id"`
	CreatedAt util.FormatTime `json:"created_at"`
	UpdatedAt util.FormatTime `json:"updated_at"`
}

type Route struct {
	Id         string   `json:"id"`
	Uri        string   `json:"uri"`        //uri TODO 先支持lb
	Predicates []string `json:"predicates"` //路径配置规则 TODO 先支持Path
	Filters    []string `json:"filters"`    //
}
