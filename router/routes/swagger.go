package routes

import (
	"github.com/cloudwego/hertz/pkg/route"
	"github.com/go-mogu/mogu-gateway/app/controller/swagger"
)

func InitSwagger(r *route.RouterGroup) (router route.IRoutes) {
	swaggerGroup := r.Group("/swagger-resources")
	{
		swaggerGroup.GET("", swagger.Swagger.SwaggerResources)
		swaggerGroup.GET("/configuration/ui", swagger.Swagger.SwaggerUi)
		swaggerGroup.GET("/configuration/security", swagger.Swagger.SwaggerSecurity)
	}
	return swaggerGroup
}
