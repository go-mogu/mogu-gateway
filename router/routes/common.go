package routes

import (
	"github.com/cloudwego/hertz/pkg/route"
	"github.com/go-mogu/mogu-gateway/app/controller/common"
)

func InitCommonGroup(r *route.RouterGroup) (router route.IRoutes) {
	commonGroup := r.Group("")
	{
		// ping
		commonGroup.GET("/ping", common.Common.Ping)
	}
	return commonGroup
}
