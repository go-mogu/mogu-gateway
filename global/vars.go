package global

import (
	"github.com/go-mogu/mogu-gateway/config"
	"github.com/spf13/viper"
)

var (
	Cfg   *config.Conf
	Viper *viper.Viper
)
