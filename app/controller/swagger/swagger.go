package swagger

import (
	"context"
	"github.com/cloudwego/hertz/pkg/app"
	"github.com/go-mogu/mogu-gateway/global"
	"github.com/go-mogu/mogu-gateway/models"
	"github.com/go-mogu/mogu-gateway/pkg/consts"
	"github.com/go-mogu/mogu-gateway/pkg/util"
	"net/http"
)

var (
	Swagger = cSwagger{}
	apiUrl  = "/v2/api-docs"
)

type cSwagger struct{}

// SwaggerResources resources
func (c *cSwagger) SwaggerResources(context context.Context, ctx *app.RequestContext) {
	res := make([]*models.SwaggerResources, 0)
	for _, route := range global.Cfg.Routes {
		uriInfo, _ := util.ParseURL(route.Uri, -1)
		name := uriInfo["host"]
		url := consts.PathSeparator + name + apiUrl
		res = append(res, &models.SwaggerResources{
			Name:           name,
			URL:            url,
			SwaggerVersion: "2.0.0",
			Location:       url,
		})

	}
	ctx.JSON(http.StatusOK, res)
}

// SwaggerUi ui
func (c *cSwagger) SwaggerUi(context context.Context, ctx *app.RequestContext) {
	res := `{"deepLinking":true,"displayOperationId":false,"defaultModelsExpandDepth":1,"defaultModelExpandDepth":1,"defaultModelRendering":"example","displayRequestDuration":false,"docExpansion":"none","filter":false,"operationsSorter":"alpha","showExtensions":false,"showCommonExtensions":false,"tagsSorter":"alpha","validatorUrl":"","supportedSubmitMethods":["get","put","post","delete","options","head","patch","trace"],"swaggerBaseUiUrl":""}`
	ctx.JSON(http.StatusOK, res)
}

// SwaggerSecurity ui
func (c *cSwagger) SwaggerSecurity(context context.Context, ctx *app.RequestContext) {
	res := `{}`
	ctx.Response.SetBodyRaw([]byte(res))
	return
}
