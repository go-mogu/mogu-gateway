package common

import (
	"context"
	"github.com/cloudwego/hertz/pkg/app"
	"github.com/go-mogu/mogu-gateway/pkg/response"
)

type cCommonController struct {
}

var Common = &cCommonController{}

// Ping 心跳
func (c *cCommonController) Ping(context context.Context, ctx *app.RequestContext) {
	response.SuccessJson(ctx, "Pong!", "")
}

// Routes 获取所有路由
func (c *cCommonController) Routes(context context.Context, ctx *app.RequestContext) {

}
